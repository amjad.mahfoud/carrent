		<br>
		<div class="container bottom" id="social">							
			<div class="text-center center-block">
				<a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
				<a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
				<a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
				<a href="mailto:test@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
			</div>
			<hr>
		</div>
		<div class="footer"></div>
		<script src="style/js/jquery-1.12.1.min.js"></script>
		<script src="style/js/jquery-ui.min.js"></script>
		<script src="style/js/bootstrap.min.js"></script>
		<script src="style/js/jquery.selectBoxIt.min.js"></script>
		<script src="style/js/front.js"></script>
	</body>
</html>