<?php   
    include 'db_connect.php';

    function getAllCars(){
        global $con;
		$getAll = $con->prepare("SELECT * FROM car ");
		$getAll->execute();
		$all = $getAll->fetchAll();
		return $all;
    }

    function getAllAvailableCars(){
        global $con;
		$getAll = $con->prepare("SELECT * FROM car where rented=0");
		$getAll->execute();
		$all = $getAll->fetchAll();
		return $all;
    }

	function getCar($id){		
        global $con;
		$car = $con->quote($id);
		$getAll = $con->prepare("SELECT * FROM car where id= {$car}");
		$getAll->execute();
		$all = $getAll->fetchAll();
		return $all;
    }

	function redirectHome(){
		header("Location:index.php");
	}

	function userExists($user){
		global $con;
		$u = $con->quote($user);
		$getAll = $con->prepare("SELECT * FROM client where user_name= {$u}");
		$getAll->execute();
		$all = $getAll->rowCount();		
		return ($all>0)?true:false;
	}

	function getTitle() {
		global $pageTitle;
		return isset($pageTitle) ? $pageTitle : 'Default';
	}
?>