<?php
	ob_start();
	session_start();
	$pageTitle = 'Home';
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';
?>
<div id="carousel-id" class="carousel slide" data-ride="carousel">	
	<div class="carousel-inner">				
		<div class="item">
			<img src="img/1.jpg" height="20px">
			<div class="container">
				<div class="carousel-caption">
					<h1>Best offers of all times</h1>
					<p>Our cars are great, cheap and beautiful</p>					
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/2.jpg" height="20px">
			<div class="container">
				<div class="carousel-caption">
					<h1>Best offers of all times</h1>
					<p>Our cars are great, cheap and beautiful</p>					
					<p><a class="btn btn-sm btn-primary" href="book.php" role="button">   Book NOW   </a></p>
				</div>
			</div>
		</div>
		<div class="item active">
			<img src="img/3.jpg" height="20px">
			<div class="container">
				<div class="carousel-caption">
					<h1>Best offers of all times</h1>
					<p>Our cars are great, cheap and beautiful</p>
					<p><a class="btn btn-sm btn-success" href="#cars" role="button">   Browse   </a></p>
				</div>
			</div>
		</div>
	</div>
	<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
<br>

<div class="container" id="cars">
    <?php
    	echo '<div class="row">';
			$allItems = getAllAvailableCars();
			foreach ($allItems as $item) {
				echo '<div class="col-sm-6 col-md-3">';
					echo '<a href="details.php?id='.$item['id'].'"> <div class="thumbnail item-box">';
						echo '<span class="price-tag">' . $item['Brand'] . ' | '. $item['Type'] .'</span>';						
						echo '<img class="img-responsive" src="cars/'. $item['img_url'] . '" alt="" />';
						echo '<div class="caption">';
							echo '<h3>' . $item['Model'] .'</h3>';
							echo '<p>' . substr($item['Description'],1,50) . '... </p>';
							echo '<div class="date">' . $item['date_added'] . '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div></a>';
			}
        echo '</div>';		
    ?>
</div>
<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>