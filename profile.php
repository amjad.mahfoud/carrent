<?php
	ob_start();
	session_start();
    $pageTitle = 'Profile';
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';
    $info;
    if (isset($_SESSION['client_id'])) {
		$getUser = $con->prepare("SELECT * FROM client WHERE id = ?");
		$getUser->execute(array($_SESSION['client_id']));
		$info = $getUser->fetch();
		//$userid = $info['UserID'];
        //var_dump($info);
    } else {
		header('Location: login.php');
		exit();
	}
?>

<h1 class="text-center">My Profile</h1>
<div class="information block">
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">My Information</div>
			<div class="panel-body">
				<ul class="list-unstyled">
					<li>
						<i class="fa fa-unlock-alt fa-fw"></i>
						<span>Login Name</span> : <?php echo $info['user_name']?>
					</li>
					<li>
						<i class="fa fa-phone fa-fw"></i>
						<span>Phone</span> : <?php echo $info['phone']?>
					</li>
                    <li>
						<i class="fa fa-envelope fa-fw"></i>
						<span>Email</span> : <?php echo $info['email']?>
					</li>
					<li>
						<i class="fa fa-user fa-fw"></i>
						<span>Full Name</span> : <?php echo $info['full_name']?>
					</li>
					<li>
						<i class="fa fa-calendar fa-fw"></i>
						<span>Registered Date</span> : <?php echo $info['reg_date']?>
					</li>					
				</ul>
				<!--<a href="#" class="btn btn-default">Edit Information</a>-->
			</div>
		</div>
	</div>
</div>
<!--Start my rented cars-->
<div id="my-ads" class="my-ads block">
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">My Contracts</div>
			<div class="panel-body">
			<?php            
                $getContracts = $con->prepare("SELECT car_id, signe_date, contract_days, price_per_day, car.Model, car.Brand ,car.img_url AS Image
                                                FROM rent_contract JOIN car
                                                ON  car.id = rent_contract.car_id
                                                WHERE client_id = ?");
                $getContracts->execute(array($_SESSION['client_id']));
                $myContracts = $getContracts->fetchAll();
                // var_dump($myContracts);
                
				if (! empty($myContracts)) {
                    echo '<div class="row">';
					foreach ($myContracts as $contract) {
						echo '<div class="col-sm-6 col-md-3">';
							echo '<div class="thumbnail item-box">';								
								echo '<span class="price-tag">Total rent: ' . ($contract['price_per_day']*$contract['contract_days']) . '</span>';
								echo '<img class="img-responsive" src="cars/'.$contract["Image"].'" alt="" />';
								echo '<div class="caption">';
									echo '<h3><a href="details.php?id='. $contract['car_id'] .'">' . $contract['Brand'] .' | '. $contract['Model'] .'</a></h3>';									
                                    $dt = new DateTime($contract['signe_date']);
									echo '<div class="date"> Rented in: ' . $dt->format('Y-m-d') .'<br>For '. $contract['contract_days'] .' day(s)</div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					}
					echo '</div>';				
				} else {
					echo 'Sorry There\'re No Contracts To Show';
				}
				
			?>
			</div>
		</div>
	</div>
</div>

<!--End my rented caars-->
<div class="my-comments block">
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">Latest Comments</div>
			<div class="panel-body">
			<?php                
                $getComments = $con->prepare("SELECT car_id, content, date_added, approved FROM comments WHERE client_id = ?");
                $getComments->execute(array($_SESSION['client_id']));
                $comments = $getComments->fetchAll();
				//var_dump($comments);
				if (!empty($comments)) {
					foreach ($comments as $comment) {
						echo '<p class="alert-info">' . $comment['content'];
                        if ($comment['approved'] == 0) { 
							echo '<span class="badge pull-right">Waiting Approval</span>'; 
						}
                        echo '</p>';
					}
				} else {
					echo 'There\'s No Comments to Show';
				}
			?>
			</div>
		</div>
	</div>
</div>
<div class="my-comments block">
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading"><span class="fa fa-envelope"></span> My Messages </div>
			<div class="panel-body">
				
						<div class='container'>
							<div class="alert nice-message"> Under Developement </div>
						</div>
				
				<a href="#"><span class="fa fa-inbox pull-right"> View All </span></a>
			</div>
		</div>
	</div>
</div>


<?php    
    include 'includes/partials/footer.php';
	ob_end_flush();
?>