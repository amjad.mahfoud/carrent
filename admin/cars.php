<?php
	ob_start();
	session_start();   
    $pageTitle = 'Cars'; 
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';    
    if(!isset($_SESSION['admin_id'])){
        header("Location:../index.php");
    }
?>
<?php
	$action = isset($_GET['action']) ? $_GET['action'] : 'Manage';
	// If The Page Is Main Page
	if ($action == 'Manage') {
		//SELECT `id`, `Model`, `Plate_Number`, `Type`, `Brand`, `date_added`, `rented`, `img_url`, `new_car`, `Description` FROM `car` WHERE 1
		?>
		<h1 class="text-center">Manage Cars</h1>
			<div class="container">
				<a href="cars.php?action=Add" class="btn btn-info">
					<i class="fa fa-plus"></i> New Car
				</a>
				<br>
				<?php
					echo '<div class="row">';
						$allItems = getAllCars();
						foreach ($allItems as $item) {
							$rented = $item['rented']? '<span class="pull-right badge">Rented</span>' : '' ;
							echo '<div class="col-sm-6 col-md-6 clearfix">';
								echo '<div class="thumbnail item-box">';
									echo '<span class="price-tag">' . $item['Brand'] . ' | '. $item['Type'] .'</span>';						
									echo '<img class="img-responsive" src="../cars/'. $item['img_url'] . '" alt="" />';
									echo '<div class="caption">';
										echo '<h3><a href="cars.php?id='.$item['id'].'"> ' . $item['Model'] .'</a></h3>';
										echo '<p>' . substr($item['Description'],1,50) . '... </p>';
										echo '<div class="date"> Date added: ' . explode(" ",$item['date_added'])[0] .$rented . '</div>';
									echo '</div>';
									echo '<div class="clearfix">';
										echo '<a href="cars.php?action=Edit&id='.$item['id'].'">';
													echo '<span class="btn btn-success pull-right">';
														echo '<i class="fa fa-edit"></i> Edit';																											
													echo '</span>';
												echo '</a>';
												echo ' <a href="cars.php?action=Delete&id='.$item['id'].'">';
													echo '<span class="btn btn-info pull-right">';
														echo '<i class="fa fa-close"></i> Delete';																											
													echo '</span>';
												echo '</a>';
									echo '</div>';	
								echo '</div>';							
							echo '</div>';
						}
					echo '</div>';		
				?>			
		</div>
	<?php } elseif ($action == 'Add') {?>
        <h1 class="text-center">Add New Car</h1>
			<div class="container">
				<form class="form-horizontal" action="?action=Insert" method="POST" enctype="multipart/form-data">

                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Model</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="Model" class="form-control" required="required" placeholder="Model" />
						</div>
					</div>

                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Brand</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="Brand" class="form-control" required="required" placeholder="Brand" />
						</div>
					</div>

                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Type</label>
						<div class="col-sm-10 col-md-10">							
							<select name="Type" id="Type" class="form-control" required="required">
								<option value="Sport">Sport</option>
								<option value="Family">Family</option>
								<option value="SUV">SUV</option>
								<option value="Luxery">Luxery</option>
							</select>							
						</div>
					</div>

                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Plate Number</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="Plate_Number" class="form-control" required="required" placeholder="Plate Number" />
						</div>
					</div>
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="Description" class="form-control" required="required" placeholder="Description Number" />
						</div>
					</div>
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Image</label>
						<div class="col-sm-10 col-md-10">
							<input type="file" name="img_url" class="form-control" required="required" placeholder="Plate Number" />
						</div>
					</div>

					<div class="form-group form-group-lg">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" value="Add Car" class="btn btn-primary btn-lg" />
						</div>
					</div>	
				</form>
			</div>
	<?php } elseif ($action == 'Delete') {
		if(isset($_GET['id']) && is_numeric($_GET['id'])){
			$id = $_GET['id'];
			echo "<div class='container'>";
			
            echo '<div class="alert nice-message">Car deleted</div>';                
            echo "</div>";
            header("refresh:4;url=cars.php");
		} else {
			echo "<div class='container'>";
            echo '<div class="alert nice-message">Wrong request</div>';                
            echo "</div>";
            header("refresh:4;url=cars.php");
		}
	} elseif ($action == 'Edit') {
		//get all info and populate to form
	?>
		<h1 class="text-center">Edit Car</h1>
			<div class="container">
				
				<div class="panel panel-info">
					<div class="panel-body">
					   <img src="../cars/bmw.jpg" class="img-responsive" alt="Image">
					</div>
				</div>				
							
				<form class="form-horizontal" action="?action=Update" method="POST" enctype="multipart/form-data">
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Model</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="Model" class="form-control" required="required" placeholder="Model" />
						</div>
					</div>
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Brand</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="Brand" class="form-control" required="required" placeholder="Brand" />
						</div>
					</div>

                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Type</label>
						<div class="col-sm-10 col-md-10">							
							<select name="Type" id="Type" class="form-control" required="required">
								<option value="Sport">Sport</option>
								<option value="Family">Family</option>
								<option value="SUV">SUV</option>
								<option value="Luxery">Luxery</option>
							</select>							
						</div>
					</div>

                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Plate Number</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="Plate_Number" class="form-control" required="required" placeholder="Plate Number" />
						</div>
					</div>
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10 col-md-10">
							<input type="textarea" name="Description" class="form-control" required="required" placeholder="Description Number" />
						</div>
					</div>
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Image</label>
						<div class="col-sm-10 col-md-10">
							<input type="file" name="img_url" class="form-control" required="required" placeholder="Plate Number" />
						</div>
					</div>
					<input type="hidden" name="id" class="form-control" value="111" />
					<div class="form-group form-group-lg">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" value="Update" class="btn btn-success btn-lg" />
						</div>
					</div>	
				</form>
			</div>
	<?php } elseif ($action == 'Insert') {
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				echo "<h1 class='text-center'>Insert Car</h1>";
				echo "<div class='container'>";
				// Get Variables From The Form	
				// 	Description Plate_Number	Type	Brand Model
				$Description 	= $_POST['Description'];
				$Plate_Number 	= $_POST['Plate_Number'];
				$Type 	= $_POST['Type'];
				$Brand 	= $_POST['Brand'];
				$Model 	= $_POST['Model'];
				// file img_url		
				// var_dump($_FILES['img_url'])	;
				//exit;
				$file_name = $_FILES['img_url']['name'];
				//$file_ext=strtolower(end(explode('.',$_FILES['img_url']['name'])));
				$file_size =$_FILES['img_url']['size'];
				$expensions= array("jpeg","jpg","png");	
				$file_tmp =$_FILES['img_url']['tmp_name'];
				// Validate The Form
				$formErrors = array();
				if (strlen($Description) < 4) {
					$formErrors[] = 'Description Cant Be Less Than <strong>4 Characters</strong>';
				}
				// if(in_array($file_ext,$expensions)=== false){
				// 	$formErrors[] = 'extension not allowed, please choose a JPEG or PNG file.';
				// }
				if($file_size > 4 * 1024 * 1024){
					$formErrors[] = 'File size must be excately 4 MB';
				}
				// Loop Into Errors Array And Echo It
				foreach($formErrors as $error) {
					echo '<div class="alert alert-danger">' . $error . '</div>';
				}
				// Check If There's No Error Proceed The Update Operation
				if (empty($formErrors)) {
					// upload 
					$name = time() . '_car_' .$file_name;
					$dist = '../cars/';
					move_uploaded_file($file_tmp,"../cars/".$name);					
					// insert
					// INSERT INTO `car`(`id`, `Model`, `Plate_Number`, `Type`, `Brand`, `date_added`, `rented`, `img_url`, `new_car`, `Description`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10])
					$stmt = $con->prepare("INSERT INTO 
						car(Model, Plate_Number, Type, Brand, rented, img_url, Description)
					VALUES(:_Model, :_Plate_Number, :_Type, :_Brand, 0, :_img_url, :_Description) ");
						$stmt->execute(array(
							'_Model' => $Model,
							'_Plate_Number' => $Plate_Number,
							'_Type' => $Type,
							'_Brand' => $Brand,
							'_img_url' => $name,
							'_Description' => $Description
						));
					echo "<div class='alert alert-success'>" . $stmt->rowCount() . ' Record Inserted</div>';					
					header("refresh:4;url=cars.php");
				}
			} else {
				echo "<div class='container'>";
				echo  '<div class="alert alert-danger">Sorry You Cant Browse This Page Directly</div>';
				header("refresh:1;url=cars.php");
				echo "</div>";
			}
			echo "</div>";
	} elseif ($action == 'Update') {
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				$id= $_POST['id'];
				$Description 	= $_POST['Description'];
				$Plate_Number 	= $_POST['Plate_Number'];
				$Type 	= $_POST['Type'];
				$Brand 	= $_POST['Brand'];
				$Model 	= $_POST['Model'];
				// file img_url		
				// var_dump($_FILES['img_url'])	;
				//exit;
				$file_name = $_FILES['img_url']['name'];
				//$file_ext=strtolower(end(explode('.',$_FILES['img_url']['name'])));
				$file_size =$_FILES['img_url']['size'];
				$expensions= array("jpeg","jpg","png");	
				$file_tmp =$_FILES['img_url']['tmp_name'];
				// Validate The Form
				$formErrors = array();
				if (strlen($Description) < 4) {
					$formErrors[] = 'Description Cant Be Less Than <strong>4 Characters</strong>';
				}
				// if(in_array($file_ext,$expensions)=== false){
				// 	$formErrors[] = 'extension not allowed, please choose a JPEG or PNG file.';
				// }
				if($file_size > 4 * 1024 * 1024){
					$formErrors[] = 'File size must be excately 4 MB';
				}
				// Loop Into Errors Array And Echo It
				foreach($formErrors as $error) {
					echo '<div class="alert alert-danger">' . $error . '</div>';
				}
				// Check If There's No Error Proceed The Update Operation
				if (empty($formErrors)) {
					// upload 
					// $name = time() . '_car_' .$file_name;
					// $dist = '../cars/';
					// move_uploaded_file($file_tmp,"../cars/".$name);					
					// // insert
					// // INSERT INTO `car`(`id`, `Model`, `Plate_Number`, `Type`, `Brand`, `date_added`, `rented`, `img_url`, `new_car`, `Description`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10])
					// $stmt = $con->prepare("UPDATE car SET
					// 	Model = :_Model ,  Plate_Number = :_Plate_Number , Type=:_Number , Brand =:_Brand ,Description=:_Description 
					// 	WHERE id = :_id ");
					// 	//TODO
					// 	$stmt->execute(array($id));
					echo "<div class='alert alert-success'>" . $stmt->rowCount() . ' Record Inserted</div>';					
					header("refresh:4;url=cars.php");
				}
		} else {
			echo "<div class='container'>";
			echo  '<div class="alert alert-danger">Sorry You Cant Browse This Page Directly</div>';
			header("refresh:1;url=cars.php");
			echo "</div>";
		}
	} else {
		echo 'Error There\'s No Page With This Name';
	} 
			
?>

<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>