<?php
	ob_start();
	session_start();    
    $pageTitle = 'Login';
	if (isset($_SESSION['admin_id'])) {
		header('Location: dashboard.php'); // Redirect To Dashboard Page
	}
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';    
?>
<?php
    $formErrors = array();
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$email = $_POST['email'];
		$password = $_POST['pass'];		
        if(login($email, $password)){
            header('Location: dashboard.php'); // Redirect To Dashboard Page
        } else {
            $formErrors[] = "Wrong user name or password";
        }
    }

?>
<form class="login col-lg-6 center" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
	<h4 class="text-center">Admin Login</h4>
	<input class="form-control" type="text" name="email" placeholder="Email" autocomplete="off" required />
	<input class="form-control" type="password" name="pass" placeholder="Password" autocomplete="new-password" required/>
	<input class="btn btn-primary btn-block" type="submit" value="Login" />
</form>
<div class="the-errors text-center">
		<?php 
			if (!empty($formErrors)) {
				foreach ($formErrors as $error) {
					echo '<div class="alert-warning">' . $error . '</div>';
				}
			}
		?>
</div>
<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>