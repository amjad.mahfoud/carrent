<?php
	ob_start();
	session_start();   
    $pageTitle = 'Clients'; 
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';    
    if(!isset($_SESSION['admin_id'])){
        header("Location:../index.php");
    }
?>
<?php
    $action = isset($_GET['action']) ? $_GET['action'] : 'Manage';
	// If The Page Is Main Page

	if ($action == 'Manage') {
        $query = '';
        if (isset($_GET['page']) && $_GET['page'] == 'Pending') {
            $query = 'AND Active = 0';
        }
        // Select All Users Except Admin 
        $stmt = $con->prepare("SELECT * FROM client WHERE user_group != 'admin' $query ORDER BY id DESC");
        // Execute The Statement
        $stmt->execute();
        // Assign To Variable 
        $rows = $stmt->fetchAll();
        if (! empty($rows)) {?>
            
        <h1 class="text-center">Manage Members</h1>
			<div class="container">
				<div class="table-responsive">
					<table class="main-table text-center table table-bordered">
						<tr>
							<td>Username</td>
							<td>Email</td>
							<td>Full Name</td>
							<td>Registered Date</td>
							<td>Control</td>
						</tr>						
						</tr>
						<?php
							foreach($rows as $row) {
								echo "<tr>";
									echo "<td>" . $row['user_name'] . "</td>";
									echo "<td>" . $row['email'] . "</td>";
									echo "<td>" . $row['full_name'] . "</td>";
									echo "<td>" . $row['reg_date'] . "</td>";
									echo "<td>
										<a href='requests.php?action=bookings&userid=" . $row['id'] . "' class='btn btn-success'><i class='fa fa-bookmark'></i> Bookings</a>
										<a href='requests.php?action=Contracts&userid=" . $row['id'] . "' class='btn btn-info confirm'><i class='fa fa-pencil'></i> Contracts</a>";
										if ($row['active'] == 0) {
											echo "<a 
													href='clients.php?action=Activate&userid=" . $row['id'] . "' 
													class='btn btn-info activate'>
													<i class='fa fa-check'></i> Activate</a>";
										}
									echo "</td>";
								echo "</tr>";
							}
						?>
						<tr>
					</table>
				</div>
				<a href="members.php?do=Add" class="btn btn-primary">
					<i class="fa fa-plus"></i> New Member
				</a>
			</div>
            <?php  }
    } elseif ($action == 'Activate') {
		//userid (isset($_GET['page']) && is_numeric($_GET['userid'])
		//echo 'activate';
		if (!(isset($_GET['userid']) && is_numeric($_GET['userid']))) {
            echo "<div class='container'>";
			echo  '<div class="alert alert-danger">Wrong request, SORRY!</div>';
			header("refresh:1;url=clients.php");
			echo "</div>";
        } else {
			$id = $_GET['userid'];							
			$stmt = $con->prepare(" UPDATE client SET active = 1 WHERE id=? ");
			$stmt->execute(array($id));
			echo "<div class='container'>";
			echo  '<div class="alert nice-message">Client activated.</div>';
			header("refresh:1;url=clients.php");
			echo "</div>";			
		}
	} else {
		echo "<div class='container'>";
		echo  '<div class="alert nice-message">Something went wrong</div>';
		echo "</div>";		
		header("refresh:1;url=clients.php");		
	}
?>
<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>