<?php
	$dsn = 'mysql:host=localhost;dbname=car_rent';
	$user = 'root';
	$pass = '';
	$option = array(
		PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
	);

	try {
		$con = new PDO($dsn, $user, $pass, $option);
		$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //var_dump($con);
        //print_r($con);
	}

	catch(PDOException $e) {
		echo 'Failed To Connect' . $e->getMessage();
	}