<?php   
    include 'db_connect.php';

    function getAllCars(){
        global $con;
		$getAll = $con->prepare("SELECT * FROM car ");
		$getAll->execute();
		$all = $getAll->fetchAll();
		return $all;
    }

    function getAllAvailableCars(){
        global $con;
		$getAll = $con->prepare("SELECT * FROM car where rented=0");
		$getAll->execute();
		$all = $getAll->fetchAll();
		return $all;
    }

	function getCar($id){		
        global $con;
		$car = $con->quote($id);
		$getAll = $con->prepare("SELECT * FROM car where id= {$car}");
		$getAll->execute();
		$all = $getAll->fetchAll();
		return $all;
    }

	function redirectHome(){
		header("Location:index.php");
	}

	// Adminstration code
	function login($email, $pass){
		global $con;
		$hashedPass = sha1($pass);
		// Check If The User Exist In Database		
		$stmt = $con->prepare("SELECT full_name, user_name, id FROM  client  WHERE 
									user_name = ?  AND  password = ? AND Active = 1 AND user_group = 'admin' ");
		$stmt->execute(array($email, $hashedPass));
		$get = $stmt->fetch();
		$count = $stmt->rowCount();
		if($count>0){			
			$_SESSION['admin_id']=$get['id'];
			$_SESSION['admin_name']=$get['full_name'];
			$_SESSION['admin_user_name']=$get['user_name'];
			return true;
		}
		return false;
	}

	function getTitle() {
		global $pageTitle;
		return isset($pageTitle) ? 'Admin | '.' '.$pageTitle : 'Admin';
	}

	function getAllFrom($field, $table, $where = NULL, $and = NULL, $orderfield, $ordering = "DESC") {
		global $con;
		$getAll = $con->prepare("SELECT $field FROM $table $where $and ORDER BY $orderfield $ordering");
		$getAll->execute();
		$all = $getAll->fetchAll();
		return $all;
	}

	/*
	 * Count Number Of Items Function v1.0
	 * Function To Count Number Of Items Rows
	 * $item = The Item To Count
	 * $table = The Table To Choose From
	 */
	function countItems($item, $table ,$where = null) {
		global $con;
		$stmt2 = $con->prepare("SELECT COUNT($item) FROM $table $where ");
		$stmt2->execute();
		return $stmt2->fetchColumn();
	}

	/*
	** Check Items Function v1.0
	** Function to Check Item In Database [ Function Accept Parameters ]
	** $select = The Item To Select [ Example: user, item, category ]
	** $from = The Table To Select From [ Example: users, items, categories ]
	** $value = The Value Of Select [ Example: Osama, Box, Electronics ]
	*/
	function checkItem($select, $from, $value) {
		global $con;
		$statement = $con->prepare("SELECT $select FROM $from WHERE $select = ?");
		$statement->execute(array($value));
		$count = $statement->rowCount();
		return $count;
	}

	/*
	** Get Latest Records Function v1.0
	** Function To Get Latest Items From Database [ Users, Items, Comments ]
	** $select = Field To Select
	** $table = The Table To Choose From
	** $order = The Desc Ordering
	** $limit = Number Of Records To Get
	*/

	function getLatest($select, $table, $order, $limit = 5 ,$where = null) {
		global $con;
		$getStmt = $con->prepare("SELECT $select FROM $table $where ORDER BY $order DESC LIMIT $limit");
		$getStmt->execute();
		$rows = $getStmt->fetchAll();
		return $rows;
	}
?>