<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"> Car rent</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav navbar-right">      
            <?php if(isset($_SESSION['admin_id'])){?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="../img/img.png" class="img-logo"/> <?php echo $_SESSION['admin_name']?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="profile.php"><i class="fa fa-gears" aria-hidden="true"></i> Settings</a></li>                
                        <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Signout</a></li>                
                    </ul>
                </li>
            <?php }?>            
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
