<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title><?php echo getTitle() ?></title>		
		<link rel="stylesheet" href="style/css/bootstrap.min.css" />
		<link rel="stylesheet" href="style/css/font-awesome.min.css" />
		<link rel="stylesheet" href="style/css/jquery-ui.css" />
		<link rel="stylesheet" href="style/css/jquery.selectBoxIt.css" />
		<link rel="stylesheet" href="style/css/backend.css" />
	</head>
	<body>
	