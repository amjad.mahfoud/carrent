<?php
	ob_start();
	session_start();    
    $pageTitle = 'Dashboard';
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';    
    if(!isset($_SESSION['admin_id'])){
        header("Location:../index.php");
    }
?>
<?php
    $numUsers = 6; // Number Of Latest Users
    $latestClients = getLatest("*", "client", "id", $numUsers , " where active = 0 AND user_group = 'client'"); // Latest Users Array
    $numItems = 6; // Number Of Latest Items
    $latestItems = getLatest("*", 'car', 'id', $numItems); // Latest Items Array
    $numComments = 6;
	$numRequests = 6;
?>
<div class="home-stats">
			<div class="container text-center">
				<div class="row">                    
					<h1 class>Dashboard</h1>										
				</div>				
				<div class="row">                    
					<div class="col-md-4">
                        <a href="clients.php">
                            <div class="stat st-members">
                                <i class="fa fa-users"></i>
                                <div class="info">
                                    Total Clients
                                    <span>
                                        <?php echo countItems('id', 'client', ' where user_group!= "admin"') ?>
                                    </span>
                                </div>
                            </div>
                        </a>
					</div>
					<div class="col-md-4">
                        <a href="clients.php?action=Manage&page=Pending">										
                            <div class="stat st-pending">
                                <i class="fa fa-user-plus"></i>
                                <div class="info">
                                    Pending Clients
                                    <span>
                                        <?php echo checkItem("active", "client", 0) ?>									
                                    </span>
                                </div>
                            </div>
                        </a>
					</div>
					<div class="col-md-4">
                        <a href="cars.php">
                            <div class="stat st-items">
                                <i class="fa fa-car"></i>
                                <div class="info">
                                    Total Cars
                                    <span>
                                        <?php echo countItems('id', 'car') ?>
                                    </span>
                                </div>
                            </div>
                        </a>
					</div>					
				</div>
				<div class="row">
					<div class="col-md-4">
                        <a href="comments.php">
                            <div class="stat st-comments">
                                <i class="fa fa-comments"></i>
                                <div class="info">
                                    Total Comments
                                    <span>
                                        <?php echo countItems('client_id', 'comments') ?>
                                    </span>
                                </div>
                            </div>
                        </a>
					</div>
					<div class="col-md-4">
                        <a href="requests.php">
                            <div class="stat st-requests">
                                <i class="fa fa-envelope"></i>
                                <div class="info">
                                    Total Not Viewed Requests
                                    <span>
                                        <?php echo countItems('id', 'booking', ' where viewed!= 1 ') ?>
                                    </span>
                                </div>
                            </div>
                        </a>
					</div>
					<div class="col-md-4">
                        <a href="contracts.php">
                            <div class="stat st-contracts">
                                <i class="fa fa-pencil-square"></i>
                                <div class="info">
                                    Total Contracts
                                    <span>
                                        <?php echo countItems('id', 'rent_contract') ?>
                                    </span>
                                </div>
                            </div>
                        </a>
					</div>
				</div>
			</div>
		</div>

        <!-- latest stff -->
        <div class="latest">
			<div class="container">
				<div class="row">
				<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-comments-o"></i> 								
								Latest <?php echo $numComments ?> Unapproved Comments 								
							</div>
							<div class="panel-body">
								<?php
									$stmt = $con->prepare("SELECT 
										comments.*, car.Brand AS Car_Brand ,car.Plate_Number, 
										client.full_name AS Member, client.id As Client_Id  
										FROM comments INNER JOIN  car  ON  car.id = comments.car_id
										INNER JOIN  client ON client.id = comments.client_id
										ORDER BY  comments.id DESC
										LIMIT $numComments");
									$stmt->execute();
									$comments = $stmt->fetchAll();
									if (!empty($comments)) {
										foreach ($comments as $comment) {
											echo '<div class="comment-box">';
												echo '<span class="member-n">
													<a href="clients.php?action=Edit&userid=' . $comment['client_id'] . '">
														' . $comment['Member'] . '</a></span>';
												echo '<p class="member-c">' . $comment['content'] . '</p>';											
											echo '</div>';
										}
									} else {
										echo 'There\'s No Comments To Show';
									}
								?>
							</div>
						</div>
					</div>				

					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-car"></i> Latest <?php echo $numItems ?> Cars 								
							</div>
							<div class="panel-body">
								<ul class="list-unstyled latest-users">
									<?php
										if (!empty($latestItems)) {											
											foreach ($latestItems as $car) {												
												echo '<li>';
												echo $car['Brand'].' -  '.$car['Model'].' - '.$car['Type'];
												echo '<a href="cars.php?action=Edit&id=' . $car['id'] . '">';
													echo '<span class="btn btn-success pull-right">';
														echo '<i class="fa fa-edit"></i> Edit';																											
													echo '</span>';
												echo '</a>';
												echo ' <a href="cars.php?action=Delete&id=' . $car['id'] . '">';
													echo '<span class="btn btn-info pull-right">';
														echo '<i class="fa fa-close"></i> Delete';																											
													echo '</span>';
												echo '</a>';
												echo '</li>';
											}
										} else {
											echo 'There\'s No Cars To Show';
										}
									?>	
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-users"></i> 
								Latest <?php echo $numUsers ?> Registerd Unapproved Users 								
							</div>
							<div class="panel-body">
								<ul class="list-unstyled latest-users">
								<?php
									if (! empty($latestClients)) {
										foreach ($latestClients as $user) {
											//var_dump($user);
											echo '<li>';
												echo $user['full_name'];
												echo '<a href="clients.php?action=details&userid=' . $user['id'] . '">';
													echo '<span class="btn btn-success pull-right">';
														echo '<i class="fa fa-info-circle"></i> Details';
														if ($user['active'] == 0) {
															echo "<a 
																	href='clients.php?action=Activate&userid=" . $user['id'] . "' 
																	class='btn btn-info pull-right activate'>
																	<i class='fa fa-check'></i> Activate</a>";
														}
													echo '</span>';
												echo '</a>';
											echo '</li>';
										}
									} else {
										echo 'There\'s No Members To Show';
									}
								?>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-car"></i> Latest <?php echo $numRequests ?> Requests
								</div>
								<div class="panel-body">
									<ul class="list-unstyled latest-users">
										<?php
									$stmt = $con->prepare("SELECT 
										booking.*, car.Brand AS Car_Brand ,car.Model, 
										client.full_name AS Member, client.id As Client_Id  
										FROM booking 
										INNER JOIN  car  ON  car.id = booking.car_id
										INNER JOIN  client ON client.id = booking.client_id
										ORDER BY  booking.id DESC
										LIMIT $numRequests");
									$stmt->execute();
									//var_dump($stmt);
									$req = $stmt->fetchAll();
									if (!empty($req)) {
										foreach ($req as $r) {
											echo '<li>';
												echo '<i class="fa fa-car"></i>' .$car['Brand'].'|'.$r['Model'];
												echo '  <i class="fa fa-user-secret"></i>: '.$r['Member'];	
												
												echo '<a class="pull-right" href="requests.php?userid=' . $r['Client_Id'] . '">';
													echo '<span class="btn btn-success pull-right">';
														echo '<i class="fa fa-info-circle"></i> View Client Bookings';
													echo '</span>';
												echo '</a>';
											echo '</li>';
										}
									} else {
										echo 'There\'s No Booking requests To Show';
									}
								?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		

<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>