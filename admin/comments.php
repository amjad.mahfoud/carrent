<?php
    /*
	================================================
	== Manage Comments Page
	== You Can Edit | Delete | Approve Comments From Here
	================================================
	*/
	ob_start();
	session_start();   
    $pageTitle = 'Comments'; 
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';    
    if(!isset($_SESSION['admin_id'])){
        header("Location:../index.php");
    }

    $action = isset($_GET['action']) ? $_GET['action'] : 'Manage';    
?>

<?php
    if ($action == 'Manage') { // Manage Comment Page
        // Select All Users Except Admin 
        $stmt = $con->prepare("SELECT 
                comments.*, car.Brand AS Car_Brand ,car.Plate_Number, 
                client.full_name AS Member, client.id As Client_Id                  
                FROM comments INNER JOIN  car  ON  car.id = comments.car_id
                INNER JOIN  client ON client.id = comments.client_id
                ORDER BY  comments.id DESC");
                // Execute The Statement
        $stmt->execute();
        // Assign To Variable 
        $comments = $stmt->fetchAll();
        //var_dump($comments);       

    if (! empty($comments)) {
    ?>
    <h1 class="text-center">Manage Comments</h1>
			<div class="container">
				<div class="table-responsive">
					<table class="main-table text-center table table-bordered">
						<tr>
							<td>ID</td>
							<td>Comment</td>
							<td>Car</td>
							<td>Client Name</td>
							<td>Added Date</td>
							<td>Control</td>
						</tr>
						<?php
							foreach($comments as $comment) {
								echo "<tr>";
									echo "<td>" . $comment['id'] . "</td>";
									echo "<td>" . $comment['content'] . "</td>";
									echo "<td>" . $comment['Car_Brand'] .' | Blate no: '. $comment['Plate_Number'] . "</td>";
									echo "<td><a href=clients.php?action=Manage&cid=".$comment['Client_Id'].">" . $comment['Member'] . "</a></td>";
									echo "<td>" . $comment['date_added'] ."</td>";
									echo "<td>										
										<a href='comments.php?action=Delete&comid=" . $comment['id'] . "' class='btn btn-danger confirm'><i class='fa fa-close'></i> Delete </a>";
										if ($comment['approved'] == 0) {
											echo "<a href='comments.php?action=Approve&comid="
													 . $comment['id'] . "' 
													class='btn btn-info activate'>
													<i class='fa fa-check'></i> Approve</a>";
										}
									echo "</td>";
								echo "</tr>";
							}
						?>
						<tr>
					</table>
				</div>
			</div>

    <?php } else {
            echo '<div class="container">';
                echo '<div class="nice-message">There\'s No Comments To Show</div>';
            echo '</div>';
        }
    } elseif ($action == 'Delete'){
        echo "<h1 class='text-center'>Delete Comment</h1>";
        echo "<div class='container'>";
        // Check If Get Request comid Is Numeric & Get The Integer Value Of It
        $comid = isset($_GET['comid']) && is_numeric($_GET['comid']) ? intval($_GET['comid']) : 0;
        // Select All Data Depend On This ID
        $check = checkItem('id', 'comments', $comid);
        // If There's Such ID Show The Form
        if ($check > 0) {
            $stmt = $con->prepare("DELETE FROM comments WHERE id = :zid");
            $stmt->bindParam(":zid", $comid);
            $stmt->execute();
            echo "<div class='alert alert-success'>" . $stmt->rowCount() . ' Record Deleted</div>';
            //redirectHome($theMsg, 'back');            
            header("refresh:4;url=comments.php");
        } else {
            echo '<div class="alert alert-danger">This ID is Not Exist</div>';            
            header("refresh:4;url=comments.php");
        }
        echo '</div>';
    } elseif ($action == 'Approve'){
        echo "<h1 class='text-center'>Approve Comment</h1>";
        echo "<div class='container'>";
            // Check If Get Request comid Is Numeric & Get The Integer Value Of It
            $comid = isset($_GET['comid']) && is_numeric($_GET['comid']) ? intval($_GET['comid']) : 0;
            // Select All Data Depend On This ID
            $check = checkItem('id', 'comments', $comid);
            // If There's Such ID Show The Form
            if ($check > 0) {
                $stmt = $con->prepare("UPDATE comments SET approved = 1 WHERE id = ?");
                $stmt->execute(array($comid));
                echo "<div class='alert alert-success'>" . $stmt->rowCount() . ' Record Approved</div>';
                header("refresh:4;url=comments.php");
            } else {
                echo '<div class="alert alert-danger">This ID is Not Exist</div>';
                header("refresh:4;url=comments.php");
            }
        echo '</div>';
    } else {        
        echo '<div class="container">';
            echo '<div class="nice-message">Wrong Page or request  <a href="dashboard.php" class="pull-right">Home</a></div>';
        echo '</div>';
    }
?>
			
<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>