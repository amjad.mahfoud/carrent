<?php 


?>
<?php
	ob_start();
	session_start();   
    $pageTitle = 'Contracts'; 
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';    
    if(!isset($_SESSION['admin_id'])){
        header("Location:../index.php");
    }
?>
<?php
	$action = isset($_GET['action']) ? $_GET['action'] : 'Manage';
	// If The Page Is Main Page
	if ($action == 'Manage') {
        echo '<h1 class="text-center">Requests</h1>';
		//SELECT `id`, `client_id`, `car_id`, `booking_date` FROM `booking` WHERE 1
        $query = '';
        if(isset($_GET['userid'])){
            $query = ' AND client.id = ' .$_GET['userid'];
        }
        $stmt = $con->prepare("SELECT booking.*, car.Brand AS Car_Brand , car.Plate_Number,
                            client.id AS Client_Id , client.full_name AS Member
                            FROM booking 
                            INNER JOIN  car  ON  car.id = booking.car_id
                            INNER JOIN  client  ON  client.id = booking.client_id
                            WHERE client.user_group != 'admin' $query AND viewed != 1
                            ORDER BY  booking.id DESC
            ");
            $stmt->execute();
            $bookings = $stmt->fetchAll();                        
            if (!empty($bookings)){ ?>
                
                <div class="container">
				<div class="table-responsive">
					<table class="main-table text-center table table-bordered">
						<tr>
							<td>Client name</td>
							<td>Car</td>							
							<td>Date</td>
                            <td>Contract Days</td>
                            <td>Price</td>
							<td>Control</td>                            
						</tr>
                            <?php
                                foreach( $bookings as $booking){
                                    $datetime = explode(" ",$booking['booking_date']);
                                    $date = $datetime[0];
                                    echo '<tr><td> <a href="requests.php?userid='.$booking['Client_Id'].'">'. $booking['Member'] . '</a></td>';
                                    echo '<td>'. $booking['Car_Brand'] . ' - ' .$booking['Plate_Number'] .'</td>';
                                    echo '<td>'. $date .'</td>';
                                    echo '<td>'.$booking['contract_days'] .'</td>';
                                    echo '<td>'.$booking['price_per_day'] * $booking['contract_days'].' SYP </td>';
                                    echo '<td>';
                                    echo "<a href='requests.php?action=sign&id=".$booking['id']."'
                                        class='btn btn-success activate'>
                                        <i class='fa fa-paperclip'></i> Sign Contract</a>";
                                    echo "<a href='requests.php?action=reject&id=".$booking['id']."'
                                        class='btn btn-danger activate'>
                                        <i class='fa fa-close'></i> Reject</a>";
                                    echo '</td></tr>';
                                }
                            ?>		
					</table>
				</div>
			</div>
            <?php } else {
                echo "<div class='container'>";
                echo '<div class="alert nice-message">No Requests Available</div>';                
                echo "</div>";
            }
	} elseif ($action == 'sign') {
        // action=sign&id=1
        if(isset($_GET['id'])){
            $contractid = $_GET['id'];
            $stmt = $con->prepare("UPDATE booking SET viewed=1 WHERE id= ?");
            $stmt->execute(array($contractid));            
            //var_dump($booking);
                $stmt = $con->prepare("SELECT booking.*, car.Brand AS Car_Brand , car.Plate_Number,
                            client.id AS Client_Id , client.full_name AS Member
                            FROM booking 
                            INNER JOIN  car  ON  car.id = booking.car_id
                            INNER JOIN  client  ON  client.id = booking.client_id
                            WHERE booking.id = ?
                            ORDER BY  booking.id DESC
                ");
                $stmt->execute(array($contractid));
                $booking = $stmt->fetch(); 
                // mark booking as viewed
                $client_id = $booking["client_id"];
                $car_id = $booking["car_id"];
                $price_per_day = $booking["price_per_day"];
                $contract_days = $booking["contract_days"];
                $stmt = $con->prepare("SELECT * FROM booking WHERE id = ?");
                $stmt->execute(array($contractid));
                // // create contract
                // $stmt = null;
                //echo 'insertingggggggggggggggggggg';
                $stmt = $con->prepare("INSERT INTO 
                    rent_contract(client_id, car_id, employee_id, contract_days, price_per_day) 
                    VALUES (:_client_id, :_car_id, 0, :_contract_days, :_price_per_day) ");
                $stmt->execute(array(
                    '_client_id'=>$client_id,
                    '_car_id'=>$car_id ,
                    '_contract_days'=>$contract_days ,
                    '_price_per_day'=>$price_per_day
                ));        
                echo "<div class='container'>";
                echo '<div class="alert nice-message">A new contract signed</div>';                
                echo "</div>";
                header("refresh:4;url=requests.php");    
        } else {
            echo "<div class='container'>";
            echo '<div class="alert nice-message">Wrong request</div>';                
            echo "</div>";
            header("refresh:4;url=requests.php");
        }
        
	} elseif ($action == 'reject') {
        if(isset($_GET['id'])){
            $contractid = $_GET['id'];
            $stmt = $con->prepare("UPDATE booking SET viewed=1 WHERE id= ?");
            $stmt->execute(array($contractid));
            echo "<div class='container'>";
            echo '<div class="alert nice-message">Request rejected</div>';                
            echo "</div>";
            header("refresh:4;url=requests.php");      
        } else {
            echo "<div class='container'>";
            echo '<div class="alert nice-message">Wrong request</div>';                
            echo "</div>";
            header("refresh:4;url=requests.php");
        }
	} else {
		echo 'Error There\'s No Page With This Name';
	}
?>
<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>