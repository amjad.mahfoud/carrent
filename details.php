<?php
	ob_start();
	session_start();
	$pageTitle = 'Details';
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';
?>
<?php
    $id;
    if(!isset($_GET['id'])){
        redirectHome();
    }
    
    $id = $_GET['id'];            
    if(null == getCar($id)){
        redirectHome();
    }
    $car = getCar($id);
    //var_dump($car);
?>
<br>
<div class="row">
  <div class="col-lg-12">
    <div class="thumbnail">    
      <img class="img-responsive" src="cars/<?php echo $car[0]["img_url"]?>" alt="" />
      <div class="caption">
        <h3><?php echo $car[0]["Brand"].'|'.$car[0]["Model"];?></h3>
        <p><?php echo $car[0]["Description"];?></p>
        <p class="clearfix">
            <a href="book.php?action=book&carid=<?php echo $id ?>" class="btn btn-warning btn-md pull-right" role="button">Book now</a>
        </p>
      </div>
    </div>
  </div>
</div>
<?php if (isset($_SESSION['client_id'])) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $comment = filter_var($_POST['comment'], FILTER_SANITIZE_STRING);
	    $carid 	= $car[0]["id"];
	    $clientid 	= $_SESSION['client_id'];
        //INSERT INTO `comments`(`car_id`, `client_id`, `content`, `approved`) 
        //VALUES ($carid, $clientid, $comment,0)
        if (!empty($comment)) {
            $stmt = $con->prepare("INSERT INTO comments(car_id, client_id, content, approved) 
						VALUES(:_carid, :_clientid, :_content, 0)");
			$stmt->execute(array(
						'_carid' => $carid,
						'_clientid' => $clientid,
						'_content' => $comment
    				));
			if ($stmt) {
                echo '<div class="alert alert-success">Comment Added</div>';
            }
        } else {
            echo '<div class="alert alert-danger">You Must Add Comment</div>';
        }
    }    
?>
	<!-- Start Add Comment -->
	<div class="row">
		<div class="col-md-offset-3">
			<div class="add-comment">
				<h3>Add Your Comment</h3>
				<form action="<?php echo $_SERVER['PHP_SELF'] . '?id=' . $car[0]['id'] ?>" method="POST">
					<textarea name="comment" height="50px" required></textarea>
					<input class="btn btn-primary" type="submit" value="Add Comment">
				</form>				
			</div>
		</div>
	</div>
    <!-- End Add Comment -->
<?php } else {
    echo '<a href="login.php">Login</a> or <a href="login.php">Register</a> To Add Comment';
} ?>
<hr class="custom-hr">
<!-- Start display comments -->
<?php
    // Select All Users Except Admin 
	$stmt = $con->prepare("SELECT comments.*, client.full_name AS Member , client.id AS client_Id
						    FROM comments INNER JOIN client 
							    ON client.id = comments.client_id
							WHERE car_id = ? AND approved = 1
							    ORDER BY comments.id DESC");
	// Execute The Statement
	$stmt->execute(array($car[0]["id"]));
	// Assign To Variable 
	$comments = $stmt->fetchAll();
    //var_dump($comments);
?>
<?php foreach ($comments as $_comment) { ?>
		<div class="comment-box">
			<div class="row">
				<div class="col-sm-2 text-center">
					<img class="img-responsive img-thumbnail img-circle center-block" src="img/img.png" alt="" />
					<a href="profile.php?id=<?php echo $_comment['client_Id'] ?>"><?php echo $_comment['Member'] ?></a>
				</div>
				<div class="col-sm-10">
					<p class="lead"><?php echo $_comment['content'] ?></p>
				</div>
			</div>
		</div>
		<hr class="custom-hr">
	<?php } ?>
<!-- end display comments -->
	
<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>