<?php
	ob_start();
	session_start();
	$pageTitle = 'Signin | Signup';
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';    
    
    // Check If User Coming From HTTP Post Request

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isset($_POST['login'])) {
			$user = $_POST['username'];
            $pass = $_POST['password'];
			$hashedPass = sha1($pass);
			// Check If The User Exist In Database
			$stmt = $con->prepare("SELECT full_name, user_name, id, user_group  FROM  client  WHERE 
										user_name = ?  AND  password = ? AND Active = 1 AND user_group = 'client'");
			$stmt->execute(array($user, $hashedPass));
			$get = $stmt->fetch();
			$count = $stmt->rowCount();
			// If Count > 0 This Mean The Database Contain Record About This Username
			if ($count > 0) {
				$_SESSION['client'] = $get['full_name']; // Register Session Name
				$_SESSION['client_id'] = $get['id']; // Register User ID in Session
				header('Location: index.php'); // Redirect To Dashboard Page
				exit();
			} else {
				$formErrors = array();
				$formErrors[] = 'Wrong user name or password';
			}
		} else {
			$formErrors = array();
			$username 	= $_POST['username'];
			$fullName 	= $_POST['fullname'];
			$password 	= $_POST['password'];
			$password2 	= $_POST['password2'];
			$email 		= $_POST['email'];
			$phone 		= $_POST['phone'];

			if (isset($username)) {
				$filterdUser = filter_var($username, FILTER_SANITIZE_STRING);
				if (strlen($filterdUser) < 4) {
					$formErrors[] = 'Username Must Be Larger Than 4 Characters';
				}
			}
			if (isset($password) && isset($password2)) {
				if (empty($password)) {
					$formErrors[] = 'Sorry Password Cant Be Empty';
				}
				if (sha1($password) !== sha1($password2)) {
					$formErrors[] = 'Sorry Password Is Not Match';
				}
			}
			if (isset($fullName)) {
				if (empty($fullName)) {
					$formErrors[] = 'Sorry User Full Name Cant Be Empty';
				}
			}

			if (isset($phone)) {
				if (empty($phone)) {
					$formErrors[] = 'Sorry phone number Cant Be Empty';
				}
			}
			if (isset($email)) {
				$filterdEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
				if (filter_var($filterdEmail, FILTER_VALIDATE_EMAIL) != true) {
					$formErrors[] = 'This Email Is Not Valid';
				}
			}
			// Check If There's No Error Proceed The User Add
			if (empty($formErrors)) {
				// Check If User Exists in Database
				$check = userExists($username);
				if ($check == 1) {
					$formErrors[] = 'Sorry This User Exists';
				} else {
					// Insert Userinfo In Database					
					$stmt = $con->prepare("INSERT INTO 
										client(full_name, phone, user_name, active,password, email)
										VALUES(:_full_name, :_phone, :_user_name, 0, :_password, :_email)");
					$stmt->execute(array(						
						':_full_name' => $fullName,
						':_phone' => $phone,
						':_user_name' => $username,
						':_password' => sha1($password),
						':_email' => $email
					));
					// Echo Success Message
					$succesMsg = 'Congrats You Are Now Registerd User';
				}
			}
		}
	}
?>

<div class="container login-page">
	<h1 class="text-center">
		<span class="selected" data-class="login">Login</span> | 
		<span data-class="signup">Signup</span>
	</h1>
	<!-- Start Login Form -->
	<form class="login" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
		<div class="input-container">
			<input 
				class="form-control" 
				type="text" 
				name="username" 
				autocomplete="off"
				placeholder="Type your username" 
				required />
		</div>
		<div class="input-container">
			<input 
				class="form-control" 
				type="password" 
				name="password" 
				autocomplete="new-password"
				placeholder="Type your password" 
				required />
		</div>
		<input class="btn btn-primary btn-block" name="login" type="submit" value="Login" />
	</form>
	<!-- End Login Form -->
	<!-- Start Signup Form -->
	<form class="signup" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
		<div class="input-container">
			<input 
				pattern=".{4,}"
				title="Username Must Be Between 4 Chars"
				class="form-control" 
				type="text" 
				name="username" 
				autocomplete="off"
				placeholder="Type your username" 
				required />
		</div>
		<div class="input-container">
			<input 
				pattern=".{4,}"
				title="Username Must Be Between 4 Chars"
				class="form-control" 
				type="text" 
				name="fullname" 
				autocomplete="off"
				placeholder="Type your name ex John Due" 
				required />
		</div>
		<div class="input-container">
			<input 
				minlength="4"
				class="form-control" 
				type="password" 
				name="password" 
				autocomplete="new-password"
				placeholder="Type a Complex password" 
				required />
		</div>
		<div class="input-container">
			<input 
				minlength="4"
				class="form-control" 
				type="password" 
				name="password2" 
				autocomplete="new-password"
				placeholder="Type a password again" 
				required />
		</div>
		<div class="input-container">
			<input 
				class="form-control" 
				type="email" 
				name="email" 
				placeholder="Type a Valid email" />
		</div>
		<div class="input-container">
			<input 
				class="form-control" 
				type="text" 
				name="phone" 
				placeholder="Type a Valid phone number" />
		</div>
		<input class="btn btn-success btn-block" name="signup" type="submit" value="Signup" />
	</form>
	<!-- End Signup Form -->
	<div class="the-errors text-center">
		<?php 
			if (!empty($formErrors)) {
				foreach ($formErrors as $error) {
					echo '<div class="msg error">' . $error . '</div>';
				}
			}
			if (isset($succesMsg)) {
				echo '<div class="msg success">' . $succesMsg . '</div>';
			}
		?>
	</div>
<?php
    include 'includes/partials/footer.php';
	ob_end_flush();
?>