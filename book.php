<?php
	ob_start();
	session_start();
    $pageTitle = 'Booking';
    include 'includes/db_connect.php';
    include 'includes/functions.php';
    include 'includes/partials/header.php';
    include 'includes/partials/navbar.php';
    // action=book&carid=3
    // check if user is loged in
    if (!isset($_SESSION['client_id'])) {
		echo "<div class='container'>";
        echo '<div class="alert nice-message">Please login first to be able to book this car <span class="fa fa-sign-in pull-right"><a href="login.php"> LOGIN</a> </span> </div>';                
        echo "</div>";        
    } else { 
        if (isset($_GET['action']) && $_GET['action'] =="Book") {
            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $contract_days 	= $_POST['contract_days'];
				$price_per_day 	= $_POST['price_per_day'];
				$car_id 	= $_POST['car_id'];
				$client_id 	= $_SESSION['client_id'];

                $formErrors = array();
				if (strlen($contract_days) > 2 || !is_numeric($contract_days)) {
					$formErrors[] = 'Contract days should be numbers less than 99';
				}
                if (!is_numeric($price_per_day) || intval($price_per_day)==0) {
					$formErrors[] = 'Price should be a numbers larger than 0';
				}
                foreach($formErrors as $error) {
					echo '<div class="alert alert-danger">' . $error . '</div>';
				}

                if (empty($formErrors)) {					
                    $stmt = $con->prepare(" INSERT INTO booking(client_id, car_id, contract_days, price_per_day) 
                                    VALUES (:_client_id, :_car_id, :_contract_days, :_price_per_day) ");
                    			$stmt->execute(array(
                                    '_price_per_day' => $price_per_day,
                                    '_contract_days' => $contract_days,
                                    '_client_id' => $client_id,
                                    '_car_id' => $car_id
                                    )
                                );
					echo "<div class='nice-message'>" . $stmt->rowCount() . ' Record Inserted</div>';					
					header("refresh:4;url=index.php");
				}

            } else {
                echo "<div class='container'>";
                echo '<div class="alert nice-message">Wrong request </div>';                
                echo "</div>";  
                header("refresh:4;url=index.php");
            }             
        }
        elseif( isset($_GET['carid']) && is_numeric($_GET['carid']) ){?>
			<div class="container">
                <h1 class="text-center">Book Car</h1>
				<form class="form-horizontal" action="?action=Book" method="POST">

                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Contract days</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="contract_days" class="form-control" required="required" placeholder="contract days" />
						</div>
					</div>
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Daily rent price</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="price_per_day" class="form-control" required="required" placeholder="Daily rent price" />
						</div>
					</div>
					<input type="hidden" name="car_id" class="form-control" value="<?php echo $_GET['carid'] ?>" />
					<div class="form-group form-group-lg">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" value="Book" class="btn btn-success btn-lg" />
						</div>
					</div>	
				</form>
			</div>

            <?php
            
        } else {
            echo "<div class='container'>";
            echo '<div class="alert nice-message">Wrong request </div>';                
            echo "</div>";  
            header("refresh:4;url=index.php");
        }

    }
?>


<?php
    
    include 'includes/partials/footer.php';
	ob_end_flush();
?>