-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2017 at 07:41 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_rent`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `booking_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contract_days` int(11) NOT NULL,
  `price_per_day` double NOT NULL,
  `viewed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `client_id`, `car_id`, `booking_date`, `contract_days`, `price_per_day`, `viewed`) VALUES
(1, 3, 2, '2017-05-29 11:01:37', 2, 0, 1),
(2, 1, 3, '2017-05-30 12:14:12', 11, 1200, 1),
(3, 1, 2, '2017-05-30 16:05:29', 0, 0, 1),
(4, 1, 2, '2017-05-30 16:05:49', 0, 0, 1),
(5, 1, 4, '2017-05-31 05:07:53', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(11) NOT NULL,
  `Model` varchar(50) NOT NULL,
  `Plate_Number` varchar(10) NOT NULL,
  `Type` varchar(25) NOT NULL,
  `Brand` varchar(25) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rented` tinyint(1) NOT NULL DEFAULT '0',
  `img_url` varchar(255) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`id`, `Model`, `Plate_Number`, `Type`, `Brand`, `date_added`, `rented`, `img_url`, `Description`, `deleted`) VALUES
(1, 'i10jjjjjjjjjjjjjjjj', '111111', 'sport', 'BMW', '2017-05-31 05:37:48', 1, 'bmw.jpg', 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 1),
(2, 'i10', '111111', 'sport', 'BMW', '2017-05-23 06:52:44', 0, 'bmw.jpg', 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 0),
(3, 'i10', '111111', 'sport', 'BMW', '2017-05-23 06:37:48', 0, 'bmw.jpg', '', 0),
(4, 'aa', '123', 'family', 'aaa', '2017-05-31 05:02:54', 0, '1496206974_car_Summer-Desktop-Wallpaper-HD.jpg', 'qwdaf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_group` enum('admin','client') NOT NULL DEFAULT 'client'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `full_name`, `phone`, `user_name`, `active`, `password`, `email`, `reg_date`, `user_group`) VALUES
(1, 'amjad', '2222', 'amjad', 1, 'da82c16684a5885d761b082d6f140d4bb869ef6f', 'ama@aa.aa', '2017-05-25 14:09:42', 'client'),
(3, 'aaaaa', 'aa', 'aaaa', 1, '3d4f2bf07dc1be38b20cd6e46949a1071f9d0e3d', 'aa@a.aa', '2017-05-25 14:09:42', 'client'),
(4, 'admin', '123', 'admin', 1, 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin@admin.com', '2017-05-29 09:03:13', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `client_messages`
--

CREATE TABLE `client_messages` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `sender_id` int(11) NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `date_seen` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `car_id`, `client_id`, `content`, `date_added`, `approved`) VALUES
(3, 2, 1, 'great carrrrrrrrrrrr\r\n', '2017-05-29 07:27:48', 1),
(4, 3, 1, 'brilliant', '2017-05-29 08:24:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rent_contract`
--

CREATE TABLE `rent_contract` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `signe_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contract_days` int(11) NOT NULL,
  `price_per_day` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- Indexes for table `client_messages`
--
ALTER TABLE `client_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rent_contract`
--
ALTER TABLE `rent_contract`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `client_messages`
--
ALTER TABLE `client_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `rent_contract`
--
ALTER TABLE `rent_contract`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
